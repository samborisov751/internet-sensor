from tkinter import *
from PIL import Image, ImageTk
import requests as r
try:
    import pymyip
except:
    pass
def getprovider():
    try:
        url2 = 'https://ipinfo.io/'+ pymyip.get_ip() + '/json'
        resp = r.get(url2)
        a = resp.json()
        _,b,_,_ = a['org'].split()
        return b
    except:
        pass
window = Tk()
window.overrideredirect(True)
window.title("ui")
window.geometry('250x50-0-0')
window.wm_attributes("-topmost", True)
window.wm_attributes("-disabled", True)
window.wm_attributes("-transparentcolor", "white")
#btn = Button(window, text="обновить",command=clicked,height = 1, width =20 )  
#btn.grid(column=1, row=5)
canvas = Canvas(window, height=50, width=50)
a = None
try:
    url = 'https://google.com'
    resp = r.get(url)
    if resp.status_code == int('200'):
        a = 'green.png'
    if resp.status_code == int('301'):
        a = 'yellow.png'
except:
    a = 'red.png'
image = Image.open(a)
photo = ImageTk.PhotoImage(image)
image = canvas.create_image(0, 0, anchor='nw',image=photo)
canvas.grid(row=0,column=1)
if a == 'red.png':
    label = Label(text=f'интернет не работает')
    label.grid(row=0, column=2)
else:
    b = getprovider()
    label = Label(text=f'ваш провайдер: {b}')
    label.grid(row=0, column=2)

window.mainloop()